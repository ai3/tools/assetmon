package assetmon

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
)

func TestClient(t *testing.T) {
	a, cleanup := createTestAssetMon(t, "")
	defer cleanup()

	srv := httptest.NewServer(HTTPAPI(a))
	defer srv.Close()

	be, err := clientutil.NewBackend(&clientutil.BackendConfig{
		URL: srv.URL,
	})
	if err != nil {
		t.Fatal(err)
	}
	defer be.Close()

	c, err := NewScanClient(be, "localhost", t.TempDir()+"/state.db")
	if err != nil {
		t.Fatal(err)
	}

	c.scanFn = func(ctx context.Context, digest string) (*Image, error) {
		return &Image{
			Digest:        digest,
			Distro:        "debian",
			DistroVersion: "bullseye",
			Artifacts: []Artifact{
				{Type: "deb", Name: "base-files", Version: "11.1+deb11u1"},
				{Type: "deb", Name: "coreutils", Version: "8.32-4+b1"},
			},
		}, nil
	}

	err = c.FullUpdate(context.Background(), []ContainerSummary{
		{
			Name:        "test-container-1",
			ImageName:   "docker.io/test-container-1:latest",
			ImageDigest: "abcdef1",
		},
	})
	if err != nil {
		t.Fatal(err)
	}
}

func TestClient_WithScanErrors(t *testing.T) {
	a, cleanup := createTestAssetMon(t, "")
	defer cleanup()

	srv := httptest.NewServer(HTTPAPI(a))
	defer srv.Close()

	be, err := clientutil.NewBackend(&clientutil.BackendConfig{
		URL: srv.URL,
	})
	if err != nil {
		t.Fatal(err)
	}
	defer be.Close()

	c, err := NewScanClient(be, "localhost", t.TempDir()+"/state.db")
	if err != nil {
		t.Fatal(err)
	}

	errDigest := "digest-err"
	scanCounts := make(map[string]int)

	c.scanFn = func(ctx context.Context, digest string) (*Image, error) {
		scanCounts[digest]++
		if digest == errDigest {
			return nil, errors.New("BAD")
		}
		return &Image{
			Digest:        digest,
			Distro:        "debian",
			DistroVersion: "bullseye",
			Artifacts: []Artifact{
				{Type: "deb", Name: "base-files", Version: "11.1+deb11u1"},
				{Type: "deb", Name: "coreutils", Version: "8.32-4+b1"},
			},
		}, nil
	}

	for i := 0; i < 10; i++ {
		if err := c.FullUpdate(context.Background(), []ContainerSummary{
			{
				Name:        fmt.Sprintf("test-container-%d", i),
				ImageName:   "docker.io/test-container-1:latest",
				ImageDigest: fmt.Sprintf("digest-%d", i),
			},
			{
				Name:        fmt.Sprintf("test-container-%d-err", i),
				ImageName:   "docker.io/test-container-1-err:latest",
				ImageDigest: errDigest,
			},
		}); err != nil {
			log.Printf("FullUpdate error: %v", err)
		}

		// Cleanup the lease locks.
		a.cleanup(time.Now().AddDate(1, 0, 0))
	}

	expectedScanCounts := map[string]int{
		"digest-0": 1,
		"digest-1": 1,
		"digest-2": 1,
		"digest-3": 1,
		"digest-4": 1,
		"digest-5": 1,
		"digest-6": 1,
		"digest-7": 1,
		"digest-8": 1,
		"digest-9": 1,
		errDigest:  maxErrors,
	}
	if !reflect.DeepEqual(scanCounts, expectedScanCounts) {
		t.Fatalf("test failed: counts=%+v, expected=%+v", scanCounts, expectedScanCounts)
	}
}
