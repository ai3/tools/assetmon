assetmon
===

This system collects information about assets within containers that
are actually running on the system. This type of data (also called
SBOM, *Software Bill Of Materials*) is usually computed at build time,
on CI systems. However the ties between *float* and CI are tenuous,
for a number of reasons:

* CI data is usually private, or anyway lacks a common API, which is a
  problem if you want to use multiple sources, possibly not directly
  controlled by you.
* it is not possible to ensure that a certain image in CI matches what
  is running in production: pushes are asynchronous with respect to
  builds, even in the presence of a Continuous Deployment system there
  are delays, or rollout failures, etc. This is complicated by the
  (common) usage of floating un-pinned image references.

There is yet another complication: there is no guarantee that an image
that is running somewhere is actually downloadable by anyone else at a
later time, as it might have already disappeared from the
registry. This forces the container scanning process to happen on the
hosts themselves, where we can bypass the registry and just operate on
the locally stored images.

Each host will thus run an *agent*, whose job is to keep the central
service up to date with information about what is running on the host.

There are two ways for the agent to upload data:

* a hook that runs on container update (still have to decide on the
  exact source), which sends an update about a single container image
* periodic full-system reports, to compensate temporary issues with
  the previous process, and delete references to containers that are
  no longer running.

All upload requests are a two-step conversation: the client starts by
sending a description of one or more images, then the server replies
with a list of those it does not already know about. The client will
then scan the missing images locally and upload the scan results.

As scans aren't exactly free, we want to minimize them by avoiding
scanning the same image multiple times on different hosts. So,
notifying the server about an image is equivalent to taking a lock for
scanning it. Locks expire after a certain time (if the scan fails for
some reason), so that the catch-up process can pick up the work if
necessary and fill the gap. This makes the service an odd mix of a
database with a (bad) task scheduling system, but maintains a very
simple data reliability model.

## Status

This package implements the agent and a centralized server for
collection and reporting, backed by a simple SQLite database. The
agent uses [Syft](https://github.com/anchore/syft) to extract
artifacts from container images.

The agent is distributed as the *assetmon* Debian package, while the
server comes in the form of a container image.

The agent only implements the cron-based approach described above,
running every 10 minutes.

## Why not use *X* instead?

There are good open-source asset-tracking / vulnerability stacks out
there, but they tend to be relatively heavyweight in terms of
deployment, and, most importantly, they tend to be very focused on the
vulnerability detection side of the issue. Vulnerability detection
isn't exactly the primary focus for this project at the moment,
furthermore there was the desire to have something lightweight that
could be easily integrated in all
[float](https://git.autistici.org/ai3/float) deployments by default.
