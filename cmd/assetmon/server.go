package main

import (
	"context"
	"flag"
	"log"
	"os"

	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/ai3/tools/assetmon"
	"github.com/google/subcommands"
	yaml "gopkg.in/yaml.v3"
)

type serverConfig struct {
	DBURI      string                   `yaml:"db_uri"`
	HTTPConfig *serverutil.ServerConfig `yaml:"http_server"`
}

func (c *serverCommand) loadConfig() (*serverConfig, error) {
	f, err := os.Open(c.configPath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	conf := serverConfig{
		DBURI: "/var/lib/assetmon/assets.db",
	}
	return &conf, yaml.NewDecoder(f).Decode(&conf)
}

type serverCommand struct {
	addr       string
	configPath string
}

func (c *serverCommand) Name() string     { return "server" }
func (c *serverCommand) Synopsis() string { return "run the asset server" }
func (c *serverCommand) Usage() string {
	return `server:
        Start the HTTP API server.

`
}

func (c *serverCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.addr, "addr", ":3798", "`address` to listen on")
	f.StringVar(&c.configPath, "config", "/etc/assetmon/server.yml", "confiugration file `path`")
}

func (c *serverCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("Too many arguments")
		return subcommands.ExitUsageError
	}

	config, err := c.loadConfig()
	if err != nil {
		log.Printf("configuration error: %v", err)
		return subcommands.ExitFailure
	}

	db, err := assetmon.OpenServiceDB(config.DBURI)
	if err != nil {
		log.Printf("database error: %v", err)
		return subcommands.ExitFailure
	}
	defer db.Close()

	a := assetmon.NewAssetMon(db)
	defer a.Close()

	if err := serverutil.Serve(assetmon.HTTPAPI(a), config.HTTPConfig, c.addr); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&serverCommand{}, "")
}
