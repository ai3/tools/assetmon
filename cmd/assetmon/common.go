package main

import (
	"flag"

	"git.autistici.org/ai3/go-common/clientutil"
)

type clientCommand struct {
	server                 string
	tlsCert, tlsKey, tlsCA string
}

func (c *clientCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.server, "server", "", "`URL` of the server")
	f.StringVar(&c.tlsCert, "tls-cert", "", "TLS certificate `path`")
	f.StringVar(&c.tlsKey, "tls-key", "", "TLS private key `path`")
	f.StringVar(&c.tlsCA, "tls-ca", "", "TLS CA `path`")
}

func (c *clientCommand) client() (clientutil.Backend, error) {
	config := &clientutil.BackendConfig{
		URL: c.server,
	}
	if c.tlsCert != "" && c.tlsKey != "" && c.tlsCA != "" {
		config.TLSConfig = &clientutil.TLSClientConfig{
			Cert: c.tlsCert,
			Key:  c.tlsKey,
			CA:   c.tlsCA,
		}
	}
	return clientutil.NewBackend(config)
}
