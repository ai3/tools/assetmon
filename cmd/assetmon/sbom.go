package main

import (
	"context"
	"encoding/json"
	"flag"
	"log"
	"os"
	"time"

	"git.autistici.org/ai3/tools/assetmon"
	"github.com/google/subcommands"
)

type sbomCommand struct {
	clientCommand
}

func (c *sbomCommand) Name() string     { return "sbom" }
func (c *sbomCommand) Synopsis() string { return "dump the SBOM for an image" }
func (c *sbomCommand) Usage() string {
	return `sbom <IMAGE_DIGEST>:
        Dump the SBOM for the specified image, in JSON syft-compatible format.

`
}

func (c *sbomCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}
	if c.server == "" {
		log.Printf("Must specify --server")
		return subcommands.ExitUsageError
	}
	imageDigest := f.Arg(0)

	be, err := c.clientCommand.client()
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	defer be.Close()

	client := assetmon.NewAPIClient(be)

	// Get the image SBOM.
	rpcCtx, cancel := context.WithTimeout(ctx, 1*time.Minute)
	defer cancel()
	image, err := client.GetImageSBOM(rpcCtx, imageDigest)
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	// Serialize.
	if err := dumpSBOM(image); err != nil {
		log.Printf("error dumping JSON output: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

type syftLikeSchema struct {
	Artifacts []assetmon.Artifact `json:"artifacts"`
	Source    struct {
		Type   string `json:"type"`
		Target struct {
			UserInput string `json:"userInput"`
			ImageID   string `json:"imageID"`
			MediaType string `json:"mediaType"`
		} `json:"target"`
	} `json:"source"`
	Descriptor struct {
		Name    string `json:"name"`
		Version string `json:"version"`
	}
	Distro struct {
		Name    string `json:"name"`
		Version string `json:"version"`
		IDLike  string `json:"idLike"`
	}
	Schema struct {
		Version string `json:"version"`
		URL     string `json:"url"`
	}
}

const (
	manifestMediaType = "application/vnd.docker.distribution.manifest.v2+json"
	syftSchemaURL     = "https://raw.githubusercontent.com/anchore/syft/main/schema/json/schema-1.1.0.json"
	syftSchemaVersion = "1.1.0"
)

func dumpSBOM(img *assetmon.Image) error {
	var s syftLikeSchema

	s.Artifacts = img.Artifacts
	s.Source.Type = "image"
	s.Source.Target.UserInput = img.Digest
	s.Source.Target.ImageID = img.Digest
	s.Source.Target.MediaType = manifestMediaType
	s.Descriptor.Name = "syft"
	s.Descriptor.Version = "[not provided]"
	s.Distro.Name = img.Distro
	s.Distro.Version = img.DistroVersion
	s.Schema.Version = syftSchemaVersion
	s.Schema.URL = syftSchemaURL

	return json.NewEncoder(os.Stdout).Encode(&s)
}

func init() {
	subcommands.Register(&sbomCommand{}, "")
}
