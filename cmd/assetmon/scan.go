package main

import (
	"context"
	"flag"
	"log"
	"os"

	"git.autistici.org/ai3/tools/assetmon"
	"github.com/google/subcommands"
)

type scanCommand struct {
	clientCommand

	hostname  string
	statePath string
}

func (c *scanCommand) Name() string     { return "scan" }
func (c *scanCommand) Synopsis() string { return "scan the running containers" }
func (c *scanCommand) Usage() string {
	return `scan:
        Scan the locally running containers.

`
}

func (c *scanCommand) SetFlags(f *flag.FlagSet) {
	c.clientCommand.SetFlags(f)
	hostname, _ := os.Hostname()
	f.StringVar(&c.hostname, "hostname", hostname, "local host `name`, for reporting")
	f.StringVar(&c.statePath, "state-db", "/var/lib/assetmon-client/state.db", "state database `path`")
}

func (c *scanCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("Too many arguments")
		return subcommands.ExitUsageError
	}
	if c.server == "" {
		log.Printf("Must specify --server")
		return subcommands.ExitUsageError
	}

	be, err := c.client()
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	defer be.Close()

	client, err := assetmon.NewScanClient(be, c.hostname, c.statePath)
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	defer client.Close()

	// Get the list of running containers.
	ctnrs, err := assetmon.RunningContainers(ctx)
	if err != nil {
		log.Printf("error fetching running containers: %v", err)
		return subcommands.ExitFailure
	}

	err = client.FullUpdate(ctx, ctnrs)
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&scanCommand{}, "")
}
