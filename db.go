package assetmon

import (
	"database/sql"

	"git.autistici.org/ai3/go-common/sqlutil"
)

var migrations = []func(*sql.Tx) error{
	sqlutil.Statement(`
CREATE TABLE images (
    digest TEXT NOT NULL,
    distro TEXT,
    distro_version TEXT,
    seen DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (digest)
);
`, `
CREATE TABLE artifacts (
    image_digest TEXT NOT NULL REFERENCES images(digest) ON DELETE CASCADE,
    name TEXT NOT NULL,
    type TEXT NOT NULL,
    version TEXT NOT NULL
);
`, `
CREATE INDEX idx_artifacts_images_digest ON artifacts(image_digest);
`, `
CREATE INDEX idx_artifacts_name_type ON artifacts(name, type);
`, `
CREATE TABLE containers (
    name TEXT NOT NULL,
    host TEXT NOT NULL,
    image_name TEXT NOT NULL,
    image_digest TEXT NOT NULL REFERENCES images(digest),
    since DATETIME NOT NULL,
    PRIMARY KEY (name, host)
);
`, `
CREATE INDEX idx_containers_name ON containers(name);
`, `
CREATE INDEX idx_containers_host ON containers(host);
`, `
CREATE INDEX idx_containers_image_digest ON containers(image_digest);
`, `
CREATE TABLE locks (
    image_digest TEXT NOT NULL,
    timestamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (image_digest)
);
`),
}

// OpenServiceDB opens a SQLite database and runs the database migrations.
func OpenServiceDB(dburi string) (*sql.DB, error) {
	return sqlutil.OpenDB(dburi, sqlutil.WithMigrations(migrations))
}

var stateMigrations = []func(*sql.Tx) error{
	sqlutil.Statement(`
CREATE TABLE state (
    digest TEXT NOT NULL,
    error_count INTEGER NOT NULL DEFAULT 0,
    last_error TEXT,
    last_error_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (digest)
);
`),
}

// OpenStateDB opens a SQLite database and runs the database migrations.
func OpenStateDB(dburi string) (*sql.DB, error) {
	return sqlutil.OpenDB(dburi, sqlutil.WithMigrations(stateMigrations))
}
