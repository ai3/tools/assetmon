package assetmon

import (
	"database/sql"
	"errors"
	"log"
	"net/http"

	"git.autistici.org/ai3/go-common/serverutil"
)

// The httpAPI just translates requests and responses from HTTP/JSON
// for the main AssetMon API.
type httpAPI struct {
	*AssetMon
}

func (h *httpAPI) handleUpdate(w http.ResponseWriter, r *http.Request) {
	var req UpdateRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	resp, err := h.Update(r.Context(), &req)
	if err != nil {
		log.Printf("Update() error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, resp)
}

func (h *httpAPI) handleFullUpdate(w http.ResponseWriter, r *http.Request) {
	var req FullUpdateRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	resp, err := h.FullUpdate(r.Context(), &req)
	if err != nil {
		log.Printf("FullUpdate() error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, resp)
}

func (h *httpAPI) handleScanReport(w http.ResponseWriter, r *http.Request) {
	var req ScanReportRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	err := h.ScanReport(r.Context(), &req)
	if err != nil {
		log.Printf("ScanReport() error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, struct{}{})
}

func (h *httpAPI) handleGetImageSBOM(w http.ResponseWriter, r *http.Request) {
	var req GetImageSBOMRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	img, err := h.GetImageSBOM(r.Context(), req.Digest)
	if errors.Is(err, sql.ErrNoRows) {
		http.NotFound(w, r)
		return
	}
	if err != nil {
		log.Printf("GetImageSBOM() error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var resp GetImageSBOMResponse
	resp.Image = *img
	serverutil.EncodeJSONResponse(w, &resp)
}

func HTTPAPI(a *AssetMon) http.Handler {
	h := &httpAPI{a}
	mux := http.NewServeMux()
	mux.HandleFunc("/api/v1/update", h.handleUpdate)
	mux.HandleFunc("/api/v1/full_update", h.handleFullUpdate)
	mux.HandleFunc("/api/v1/scan_report", h.handleScanReport)
	mux.HandleFunc("/api/v1/get_image_sbom", h.handleGetImageSBOM)

	mux.HandleFunc("/", h.handleDebugIndex)
	mux.HandleFunc("/image", h.handleDebugImageDetails)

	return mux
}
