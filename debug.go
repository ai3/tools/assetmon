package assetmon

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"regexp"
	"strings"
)

var (
	headerHTML = `<!DOCTYPE html>
<html>
<head>
 <title>Assets</title>
</head>
<body>
`

	indexHTML = headerHTML + `
<h1>Asset tracking</h1>

<p>
  Tracking {{.NumArtifacts}} artifacts across {{.NumContainers}} containers.
</p>

<p>
  <form action="" method="get">
    <input type="text" name="q" value="{{.SearchQuery}}" size="40" autofocus>
    <button type="submit">Search</button>
  </form>
</p>

{{if .QueryError}}
<p style="color:red;">
  Query error: {{.QueryError}}
</p>
{{end}}

<p>
  <small>
    Search artifacts using a whitespace-separated list of queries, each following the syntax
    <i>attribute</i> <i>&lt;operator&gt;</i> <i>value</i>. Attributes can refer to the artifact
    or to the container it belongs to, and include <i>pkg:name</i>, <i>pkg:type</i>,
    <i>pkg:version</i> for package artifacts, <i>container:name</i>, <i>container:host</i>,
    <i>container:image_digest</i> for containers. Operators are the normal SQL binary operators
    (=, !=, &lt;, &gt;, etc), with the addition of ~ to signify the LIKE operator.
  </small>
</p>

<hr>

{{if .Containers}}
<h3>Containers</h3>

<table class="table">
{{range .Containers}}
  <tr>
    <td>
      {{.Name}}
    </td>
    <td>
      <a href="/?q=container%3Ahost%3D{{.Host}}">{{.Host}}</a>
    </td>
    <td>
      <a href="/image?digest={{.ImageDigest}}">{{.ImageName}}</a>
    </td>
    <td>
      {{.Since.Format "2006-01-02 15:04:05"}}
    </td>
  </tr>
{{end}}
</table>
{{end}}

{{if .Artifacts}}
<h3>Artifacts</h3>

<table class="table">
{{range .Artifacts}}
  <tr>
    <td>{{.Type}}</td>
    <td>{{.Artifact.Name}}</td>
    <td>{{.Version}}</td>
    <td>{{.ContainerInfo.Name}}</td>
    <td>
      <a href="/image?digest={{.ImageDigest}}">{{.ImageName}}</a>
    </td>
    <td>{{.Host}}</td>
  </tr>
{{end}}
</table>
{{end}}

</body>
</html>`

	indexTemplate = template.Must(template.New("index").Parse(indexHTML))

	imageHTML = headerHTML + `
<h1>Image {{.Image.Digest}}</h1>

{{if .Image.Distro}}
<p>
  Distribution: <i>{{.Image.Distro}}{{if .Image.DistroVersion}}/{{.Image.DistroVersion}}{{end}}</i>
</p>
{{end}}

<h3>Artifacts</h3>

<table class="table">
{{range .Image.Artifacts}}
  <tr>
    <td>{{.Type}}</td>
    <td>
      <a href="/?q=pkg%3Atype%3D{{.Type}}+pkg%3Aname%3D{{.Name}}">{{.Name}}</a>
    </td>
    <td>
      <a href="/?q=pkg%3Atype%3D{{.Type}}+pkg%3Aname%3D{{.Name}}+pkg%3Aversion%3D{{.Version}}">{{.Version}}</a>
    </td>
  </tr>
{{end}}
</table>

<h3>Containers</h3>

<table class="table">
{{range .Containers}}
  <tr>
    <td>
      {{.Name}}
    </td>
    <td>
      <a href="/?q=container%3Ahost%3D{{.Host}}">{{.Host}}</a>
    </td>
    <td>
      {{.ImageName}}
    </td>
    <td>
      {{.Since.Format "2006-01-02 15:04:05"}}
    </td>
  </tr>
{{end}}
</table>

<p><a href="/">Home</a></p>

</body>
</html>`

	imageTemplate = template.Must(template.New("image").Parse(imageHTML))
)

var (
	digestRx = regexp.MustCompile(`^sha256:[a-f0-9]{64}$`)
	tokenRx  = regexp.MustCompile(`^([a-z:_]+)?(=|>|<|>=|<=|~|!=)?(.*)$`)
)

func looksLikeDigest(s string) bool {
	return digestRx.MatchString(s)
}

func parseQuery(s string, tokenFn func(string, string, string) error) error {
	for _, token := range strings.Fields(s) {
		parsed := tokenRx.FindStringSubmatch(token)
		if parsed == nil {
			return fmt.Errorf("query syntax error in '%s'", token)
		}

		if err := tokenFn(parsed[1], parsed[2], parsed[3]); err != nil {
			return fmt.Errorf("query syntax error in '%s': %w", token, err)
		}
	}
	return nil
}

// Parse the query, and fill both a container deployment request and
// an artifacts request at the same time. We'll decide later which one
// to use.
func parseQueryToken(creq *GetContainerDeploymentRequest, areq *FindArtifactsRequest) func(string, string, string) error {
	return func(field, op, value string) error {
		if field == "" {
			field = "container:name"
		}
		if op == "" {
			op = "="
		}

		value = strings.ToLower(value)
		if !strings.Contains(field, ":") {
			field = "container:" + field
		}

		if strings.HasPrefix(field, "container:") {
			switch field[10:] {
			case "name":
				creq.Name = value
			case "digest":
				creq.ImageDigest = value
			case "host":
				creq.Host = value
			default:
				return fmt.Errorf("unknown container field '%s'", field)
			}
		}

		areq.Match = append(areq.Match, Match{Field: field, Op: op, Value: value})
		return nil
	}
}

func (h *httpAPI) handleDebugIndex(w http.ResponseWriter, req *http.Request) {
	numArtifacts, numContainers, _ := h.stats(req.Context())

	var getReq GetContainerDeploymentRequest
	var artReq FindArtifactsRequest
	searchQuery := req.FormValue("q")
	if looksLikeDigest(searchQuery) {
		http.Redirect(w, req, "/image?digest="+searchQuery, http.StatusTemporaryRedirect)
		return
	}
	queryError := parseQuery(searchQuery, parseQueryToken(&getReq, &artReq))

	var containers []*ContainerInfo
	var artifacts []*ArtifactResult

	if queryError == nil {
		if artReq.empty() {
			containers, queryError = h.GetContainerDeployment(req.Context(), &getReq)
		} else {
			resp, err := h.FindArtifacts(req.Context(), &artReq)
			if err != nil {
				queryError = err
			} else {
				artifacts = resp.Results
			}
		}
	}

	w.Header().Set("Cache-Control", "no-store")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	if err := indexTemplate.Execute(w, map[string]interface{}{
		"SearchQuery":   searchQuery,
		"QueryError":    queryError,
		"NumArtifacts":  numArtifacts,
		"NumContainers": numContainers,
		"Containers":    containers,
		"Artifacts":     artifacts,
	}); err != nil {
		log.Printf("template error for %s: %v", req.URL.Path, err)
	}
}

func (h *httpAPI) handleDebugImageDetails(w http.ResponseWriter, req *http.Request) {
	digest := req.FormValue("digest")
	image, err := h.GetImageSBOM(req.Context(), digest)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	containers, err := h.GetContainerDeployment(req.Context(), &GetContainerDeploymentRequest{
		ImageDigest: digest,
	})
	if err != nil {
		log.Printf("GetContainerDeployment() error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Cache-Control", "no-store")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	if err := imageTemplate.Execute(w, map[string]interface{}{
		"Image":      image,
		"Containers": containers,
	}); err != nil {
		log.Printf("template error for %s: %v", req.URL.Path, err)
	}
}
