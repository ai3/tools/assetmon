package assetmon

import (
	"context"
	"os"
	"testing"
)

func createTestAssetMon(t *testing.T, testDataFile string) (*AssetMon, func()) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}

	db, err := OpenServiceDB(dir + "/assets.db")
	if err != nil {
		t.Fatal(err)
	}

	if testDataFile != "" {
		data, err := os.ReadFile(testDataFile)
		if err != nil {
			t.Fatal(err)
		}
		if _, err := db.Exec(string(data)); err != nil {
			t.Fatal("loading test data:", err)
		}
	}

	a := NewAssetMon(db)

	return a, func() {
		a.Close()
		db.Close()
		os.RemoveAll(dir)
	}
}

func TestService_Update(t *testing.T) {
	a, cleanup := createTestAssetMon(t, "")
	defer cleanup()

	resp, err := a.Update(context.Background(), &UpdateRequest{
		Host: "host1",
		Container: ContainerSummary{
			Name:        "test-container",
			ImageName:   "docker.io/test-container:latest",
			ImageDigest: "abcdef",
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	if len(resp.ScanImages) == 0 {
		t.Fatal("service did not assign scan")
	}
}

func TestService_Lock(t *testing.T) {
	a, cleanup := createTestAssetMon(t, "")
	defer cleanup()

	req := &UpdateRequest{
		Host: "host1",
		Container: ContainerSummary{
			Name:        "test-container",
			ImageName:   "docker.io/test-container:latest",
			ImageDigest: "abcdef",
		},
	}
	resp, err := a.Update(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	if len(resp.ScanImages) == 0 {
		t.Fatal("service did not assign scan")
	}

	// Making another similar request should result in no action,
	// because the scanning lock was taken by the previous request.
	resp, err = a.Update(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	if len(resp.ScanImages) > 0 {
		t.Fatal("service assigned scan twice, locking failure")
	}
}

func TestService_FullUpdate(t *testing.T) {
	a, cleanup := createTestAssetMon(t, "")
	defer cleanup()

	resp, err := a.FullUpdate(context.Background(), &FullUpdateRequest{
		Host: "host1",
		Containers: []ContainerSummary{
			{
				Name:        "test-container-1",
				ImageName:   "docker.io/test-container-1:latest",
				ImageDigest: "abcdef1",
			},
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	if len(resp.ScanImages) == 0 {
		t.Fatal("service did not assign scan")
	}

	resp, err = a.FullUpdate(context.Background(), &FullUpdateRequest{
		Host: "host1",
		Containers: []ContainerSummary{
			{
				Name:        "test-container-2",
				ImageName:   "docker.io/test-container-2:latest",
				ImageDigest: "abcdef2",
			},
			{
				Name:        "test-container-3",
				ImageName:   "docker.io/test-container-3:latest",
				ImageDigest: "abcdef3",
			},
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	if len(resp.ScanImages) != 2 {
		t.Fatalf("service did not assign scans: %v", resp.ScanImages)
	}

	containers, err := a.GetContainerDeployment(context.Background(), &GetContainerDeploymentRequest{})
	if err != nil {
		t.Fatal(err)
	}
	if len(containers) != 2 {
		t.Fatalf("old container was not removed: %+v", containers)
	}
}

func TestService_FindArtifacts_Pkg(t *testing.T) {
	a, cleanup := createTestAssetMon(t, "testdata/artifacts.sql")
	defer cleanup()

	resp, err := a.FindArtifacts(context.Background(), &FindArtifactsRequest{
		Match: []Match{
			{
				Field: "pkg:name",
				Op:    "=",
				Value: "base-files",
			},
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	if n := len(resp.Results); n != 3 {
		t.Errorf("wrong number of results: got %d, expected 3", n)
	}
}

func TestService_FindArtifacts_Container(t *testing.T) {
	a, cleanup := createTestAssetMon(t, "testdata/artifacts.sql")
	defer cleanup()

	resp, err := a.FindArtifacts(context.Background(), &FindArtifactsRequest{
		Match: []Match{
			{
				Field: "container:name",
				Op:    "=",
				Value: "bar-service",
			},
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	if n := len(resp.Results); n != 3 {
		t.Fatalf("wrong number of results: got %d, expected 3", n)
	}
}

func TestService_GetSBOM(t *testing.T) {
	a, cleanup := createTestAssetMon(t, "testdata/artifacts.sql")
	defer cleanup()

	img, err := a.GetImageSBOM(context.Background(), "abcdef")
	if err != nil {
		t.Fatal(err)
	}
	if n := len(img.Artifacts); n != 4 {
		t.Errorf("wrong number of results: got %d, expected 3", n)
	}
}
