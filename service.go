package assetmon

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"git.autistici.org/ai3/go-common/sqlutil"
)

var (
	lockTimeout     = 10 * time.Minute
	cleanupInterval = 1 * time.Minute
)

type Artifact struct {
	Type    string `json:"type"`
	Name    string `json:"name"`
	Version string `json:"version"`
}

type Image struct {
	Digest        string     `json:"digest"`
	Distro        string     `json:"distro"`
	DistroVersion string     `json:"distro_version"`
	Artifacts     []Artifact `json:"artifacts"`
}

type Container struct {
	Name  string `json:"name"`
	Image Image  `json:"image"`
}

type ContainerSummary struct {
	Name        string `json:"name"`
	ImageName   string `json:"image_name"`
	ImageDigest string `json:"image_digest"`
}

type ContainerInfo struct {
	ContainerSummary

	Host  string    `json:"host"`
	Since time.Time `json:"running_since"`
}

type UpdateRequest struct {
	Host      string           `json:"host"`
	Container ContainerSummary `json:"container"`
}

type FullUpdateRequest struct {
	Host       string             `json:"host"`
	Containers []ContainerSummary `json:"containers"`
}

type UpdateResponse struct {
	ScanImages []string `json:"scan_images"`
}

type ScanReportRequest struct {
	Image *Image `json:"image"`
}

type GetContainerDeploymentRequest struct {
	Name        string `json:"name"`
	Host        string `json:"host"`
	ImageDigest string `json:"image_digest"`
}

type GetContainerDeploymentResponse struct {
	Containers []*ContainerInfo `json:"containers"`
}

type GetImageSBOMRequest struct {
	Digest string `json:"digest"`
}

type GetImageSBOMResponse struct {
	Image
}

type Match struct {
	Field string `json:"field"`
	Op    string `json:"op"`
	Value string `json:"value"`
}

type FindArtifactsRequest struct {
	Match []Match `json:"match"`
}

func (r *FindArtifactsRequest) empty() bool {
	return len(r.Match) == 0
}

type ArtifactResult struct {
	Artifact
	ContainerInfo
}

type FindArtifactsResponse struct {
	Results []*ArtifactResult `json:"results"`
}

// AssetMon implements the asset management (tracking) service.
type AssetMon struct {
	db *sql.DB

	stopCh chan bool
}

func NewAssetMon(db *sql.DB) *AssetMon {
	a := &AssetMon{
		db:     db,
		stopCh: make(chan bool),
	}
	go a.cleanupThread()
	return a
}

func (a *AssetMon) Close() {
	close(a.stopCh)
}

func (a *AssetMon) GetImageSBOM(ctx context.Context, digest string) (*Image, error) {
	img := Image{Digest: digest}
	err := sqlutil.WithReadonlyTx(ctx, a.db, func(tx *sql.Tx) error {
		// Find the image.
		err := tx.QueryRow("SELECT distro, distro_version FROM images WHERE digest = ?", digest).Scan(
			&img.Distro, &img.DistroVersion)
		if err != nil {
			return err
		}

		// Fetch all associated artifacts.
		rows, err := tx.Query("SELECT type, name, version FROM artifacts WHERE image_digest = ? ORDER BY type ASC, name ASC", digest)
		if err != nil {
			return err
		}
		defer rows.Close()
		for rows.Next() {
			var artifact Artifact
			if err := rows.Scan(&artifact.Type, &artifact.Name, &artifact.Version); err != nil {
				return err
			}
			img.Artifacts = append(img.Artifacts, artifact)
		}
		return rows.Err()
	})
	return &img, err
}

func (a *AssetMon) GetContainerDeployment(ctx context.Context, req *GetContainerDeploymentRequest) ([]*ContainerInfo, error) {
	// Build the query depending on the filters passed in the request.
	q := sqlutil.NewQuery("SELECT name, host, image_name, image_digest, since FROM containers").OrderBy("name ASC, host ASC")
	if req.Host != "" {
		q = q.Where("host = ?", req.Host)
	}
	if req.Name != "" {
		q = q.Where("name = ?", req.Name)
	}
	if req.ImageDigest != "" {
		q = q.Where("image_digest = ?", req.ImageDigest)
	}

	var out []*ContainerInfo
	err := sqlutil.WithReadonlyTx(ctx, a.db, func(tx *sql.Tx) error {
		rows, err := q.Query(tx)
		if err != nil {
			return err
		}
		defer rows.Close()
		for rows.Next() {
			var ci ContainerInfo
			if err := rows.Scan(&ci.Name, &ci.Host, &ci.ImageName, &ci.ImageDigest, &ci.Since); err != nil {
				return err
			}
			out = append(out, &ci)
		}
		return rows.Err()
	})
	return out, err
}

func addMatchToQuery(q *sqlutil.QueryBuilder, match *Match) (*sqlutil.QueryBuilder, error) {
	var field string
	switch match.Field {
	case "pkg:name", "pkg:type", "pkg:version":
		field = "a." + match.Field[4:]
	case "container:name", "container:host", "container:image_digest":
		field = "c." + match.Field[10:]
	default:
		return nil, fmt.Errorf("invalid match field '%s'", match.Field)
	}

	op := match.Op
	value := match.Value
	switch op {
	case "=", "<", ">", "<=", ">=", "!=":
	case "~":
		op = "LIKE"
		value = strings.Replace(value, "*", "%", -1)
	default:
		return nil, fmt.Errorf("invalid match operator '%s'", op)
	}

	return q.Where(fmt.Sprintf("%s %s ?", field, op), value), nil
}

func (a *AssetMon) FindArtifacts(ctx context.Context, req *FindArtifactsRequest) (*FindArtifactsResponse, error) {
	q := sqlutil.NewQuery("SELECT a.name, a.type, a.version, a.image_digest, c.name, c.host, c.image_name, c.since FROM artifacts AS a JOIN containers AS c ON a.image_digest = c.image_digest").OrderBy("a.type ASC, a.name ASC, c.name ASC, c.host ASC")
	for i := 0; i < len(req.Match); i++ {
		var err error
		q, err = addMatchToQuery(q, &req.Match[i])
		if err != nil {
			return nil, err
		}
	}

	var resp FindArtifactsResponse
	err := sqlutil.WithReadonlyTx(ctx, a.db, func(tx *sql.Tx) error {
		rows, err := q.Query(tx)
		if err != nil {
			return err
		}
		defer rows.Close()
		for rows.Next() {
			var r ArtifactResult
			if err := rows.Scan(&r.Artifact.Name, &r.Type, &r.Version, &r.ImageDigest, &r.ContainerInfo.Name, &r.Host, &r.ImageName, &r.Since); err != nil {
				return err
			}
			resp.Results = append(resp.Results, &r)
		}
		return rows.Err()
	})
	return &resp, err
}

func (a *AssetMon) updateContainer(ctx context.Context, host string, c ContainerSummary) (bool, error) {
	// Record that this container is now running.
	err := sqlutil.WithTx(ctx, a.db, func(tx *sql.Tx) error {
		_, err := tx.Exec("INSERT OR REPLACE INTO containers (name, host, image_name, image_digest, since) VALUES (?, ?, ?, ?, ?)", c.Name, host, c.ImageName, c.ImageDigest, time.Now())
		return err
	})
	if err != nil {
		return false, err
	}

	scan := true
	err = sqlutil.WithTx(ctx, a.db, func(tx *sql.Tx) error {
		// See if we have results for this image, otherwise
		// take a lock and return a request to scan
		// it. Funnily enough we can't simply use row.Err()
		// but must actually call Scan().
		var tmp int
		err := tx.QueryRow("SELECT 1 FROM images WHERE digest = ?", c.ImageDigest).Scan(&tmp)
		if err == nil {
			// All ok, we already got it.
			return errors.New("already have it")
		}

		// Try to insert a lock. If the lock is already taken,
		// the transaction will fail to commit.
		_, err = tx.Exec("INSERT INTO locks (image_digest) VALUES (?)", c.ImageDigest)
		return err
	})
	if err != nil {
		scan = false
	}
	return scan, nil
}

func (a *AssetMon) Update(ctx context.Context, req *UpdateRequest) (*UpdateResponse, error) {
	var resp UpdateResponse
	acquired, err := a.updateContainer(ctx, req.Host, req.Container)
	if err != nil {
		return nil, err
	}
	if acquired {
		resp.ScanImages = append(resp.ScanImages, req.Container.ImageDigest)
	}
	return &resp, nil
}

func (a *AssetMon) FullUpdate(ctx context.Context, req *FullUpdateRequest) (*UpdateResponse, error) {
	// Get the list of containers running on this host, and store
	// them so we can later remove the ones no longer running.
	oldContainers := make(map[string]struct{})
	err := sqlutil.WithTx(ctx, a.db, func(tx *sql.Tx) error {
		rows, err := tx.Query("SELECT name FROM containers WHERE host = ?", req.Host)
		if err != nil {
			return err
		}
		defer rows.Close()
		for rows.Next() {
			var name string
			if err := rows.Scan(&name); err != nil {
				return err
			}
			oldContainers[name] = struct{}{}
		}
		return rows.Err()
	})
	if err != nil {
		return nil, err
	}

	// Iterate over the request containers.
	var resp UpdateResponse
	for _, c := range req.Containers {
		delete(oldContainers, c.Name)
		acquired, err := a.updateContainer(ctx, req.Host, c)
		if err != nil {
			return nil, err
		}
		if acquired {
			resp.ScanImages = append(resp.ScanImages, c.ImageDigest)
		}
	}

	// Delete the containers that are no longer running.
	err = sqlutil.WithTx(ctx, a.db, func(tx *sql.Tx) error {
		for name := range oldContainers {
			if _, err := tx.Exec("DELETE FROM containers WHERE name = ?", name); err != nil {
				return err
			}
		}
		return nil
	})

	return &resp, err
}

func (a *AssetMon) ScanReport(ctx context.Context, req *ScanReportRequest) error {
	return sqlutil.WithTx(ctx, a.db, func(tx *sql.Tx) error {
		_, err := tx.Exec("INSERT INTO images (digest, distro, distro_version) VALUES (?, ?, ?)", req.Image.Digest, req.Image.Distro, req.Image.DistroVersion)
		if err != nil {
			return err
		}

		// Let's use a prepared statement, as there might be a
		// lot of artifacts to load.
		stmt, err := tx.Prepare("INSERT INTO artifacts (image_digest, type, name, version) VALUES (?, ?, ?, ?)")
		if err != nil {
			return err
		}
		defer stmt.Close()
		for _, art := range req.Image.Artifacts {
			_, err = stmt.Exec(req.Image.Digest, art.Type, art.Name, art.Version)
			if err != nil {
				return err
			}
		}

		// Clear the lock associated with this scan.
		_, err = tx.Exec("DELETE FROM locks WHERE image_digest = ?", req.Image.Digest)
		return err
	})
}

func (a *AssetMon) cleanupThread() {
	tick := time.NewTicker(cleanupInterval)
	defer tick.Stop()
	for {
		select {
		case t := <-tick.C:
			if err := a.cleanup(t); err != nil {
				log.Printf("cleanup error: %v", err)
			}
		case <-a.stopCh:
			return
		}
	}
}

// Periodic cleanup of obsolete entries in the database.
func (a *AssetMon) cleanup(t time.Time) error {
	return sqlutil.WithTx(context.Background(), a.db, func(tx *sql.Tx) error {
		// Clear up abandoned locks.
		cutoff := t.Add(-lockTimeout)
		_, err := tx.Exec("DELETE FROM locks WHERE timestamp < ?", cutoff)
		if err != nil {
			return err
		}

		// Remove image scan results that are no longer
		// referenced by a running container, and remove all
		// their artifacts as well (thanks to ON DELETE
		// CASCADE on the foreign key relationship).
		_, err = tx.Exec("DELETE FROM images WHERE digest NOT IN (SELECT image_digest FROM containers)")
		return err
	})
}

func (a *AssetMon) stats(ctx context.Context) (numArtifacts int, numContainers int, err error) {
	err = sqlutil.WithReadonlyTx(ctx, a.db, func(tx *sql.Tx) error {
		if err := tx.QueryRow("SELECT COUNT(*) FROM artifacts").Scan(&numArtifacts); err != nil {
			return err
		}
		return tx.QueryRow("SELECT COUNT(*) FROM containers").Scan(&numContainers)
	})
	return
}
