
INSERT INTO images (digest, distro, distro_version) VALUES
  ('abcdef', 'debian', 'bullseye'),
  ('123456', 'debian', 'bullseye');

INSERT INTO containers (name, host, image_name, image_digest, since) VALUES
  ('foo-service', 'host1', 'foo:master', 'abcdef', '2021-03-21 10:00:00'),
  ('foo2-service', 'host2', 'foo:master', 'abcdef', '2021-04-21 18:00:00'),
  ('bar-service', 'host1', 'bar:latest', '123456', '2021-04-17 16:00:00');

INSERT INTO artifacts (image_digest, name, type, version) VALUES
  ('abcdef', 'libc6:amd64', 'deb', '2.31-13+deb11u2'),
  ('abcdef', 'coreutils', 'deb', '8.32-4+b1'),
  ('abcdef', 'base-files', 'deb', '11.1+deb11u1'),
  ('abcdef', 'banana', 'deb', '10.0.0'),
  ('123456', 'libc6:amd64', 'deb', '2.31-13+deb11u2'),
  ('123456', 'coreutils', 'deb', '8.32-4+b1'),
  ('123456', 'base-files', 'deb', '11.1+deb11u1');
