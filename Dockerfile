FROM golang:1.21.0 as build
ADD . /src
RUN cd /src && \
    go build -ldflags="-extldflags=-static" -tags "sqlite_omit_load_extension netgo" -o assetmon ./cmd/assetmon && \
    strip assetmon

FROM scratch
COPY --from=build /src/assetmon /assetmon

ENTRYPOINT ["/assetmon", "server"]

