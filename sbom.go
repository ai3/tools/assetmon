package assetmon

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
)

// Type used to parse the syft JSON output.
type sbom struct {
	Artifacts []Artifact `json:"artifacts"`
	Distro    struct {
		Name    string `json:"name"`
		Version string `json:"version"`
	} `json:"distro"`
}

func (s *sbom) toImage(digest string) *Image {
	return &Image{
		Digest:        digest,
		Distro:        s.Distro.Name,
		DistroVersion: s.Distro.Version,
		Artifacts:     s.Artifacts,
	}
}

// Run a command and decode its JSON output.
func runJSONCmd(cmd *exec.Cmd, obj interface{}) error {
	cmd.Stderr = os.Stderr
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}
	if err := cmd.Start(); err != nil {
		return err
	}
	if err := json.NewDecoder(stdout).Decode(obj); err != nil {
		return err
	}
	return cmd.Wait()
}

func scanImage(ctx context.Context, digest string) (*Image, error) {
	// We have the digest, but we need to find the image ID for
	// "podman save".
	imgID, err := findImageIDByDigest(ctx, digest)
	if err != nil {
		return nil, err
	}

	// Run podman save | syft (with appropriate i/o class suitable for
	// a background operation, and using environment vars to configure
	// Syft disabling update checks).
	pipe := fmt.Sprintf("podman save --format=oci-archive %s | syft --quiet -o json oci-archive:/dev/fd/0", imgID)
	cmd := exec.CommandContext(ctx, "ionice", "-c", "3", "env", "SYFT_CHECK_FOR_APP_UPDATE=false", "sh", "-c", pipe)

	var result sbom
	if err := runJSONCmd(cmd, &result); err != nil {
		return nil, err
	}
	return result.toImage(digest), nil
}

type podmanImg struct {
	ID     string `json:"Id"`
	Digest string
}

func findImageIDByDigest(ctx context.Context, digest string) (string, error) {
	var imgs []podmanImg
	cmd := exec.CommandContext(ctx, "podman", "image", "ls", "--format=json")
	if err := runJSONCmd(cmd, &imgs); err != nil {
		return "", fmt.Errorf("error listing container images: %w", err)
	}

	for _, img := range imgs {
		if img.Digest == digest {
			return img.ID, nil
		}
	}
	return "", fmt.Errorf("couldn't find image with digest %s", digest)
}

// RunningContainers returns the list of currently running (according
// to Podman) containers, along with their associated image IDs.
func RunningContainers(ctx context.Context) ([]ContainerSummary, error) {
	// In order to build the container map, we have to run a
	// couple of podman commands to get the image digests instead
	// of podman's internal image IDs.
	var ctnrs []struct {
		Names   []string
		Image   string
		ImageID string
	}

	cmd := exec.CommandContext(ctx, "podman", "ps", "--format=json", "--filter=status=running")
	if err := runJSONCmd(cmd, &ctnrs); err != nil {
		return nil, fmt.Errorf("error listing running containers: %w", err)
	}

	var imgs []podmanImg
	cmd = exec.CommandContext(ctx, "podman", "image", "ls", "--format=json")
	if err := runJSONCmd(cmd, &imgs); err != nil {
		return nil, fmt.Errorf("error listing container images: %w", err)
	}
	imgDigestByID := make(map[string]string)
	for _, i := range imgs {
		imgDigestByID[i.ID] = i.Digest
	}

	// Now build the ContainerSummary map.
	out := make([]ContainerSummary, 0, len(ctnrs))
	for _, c := range ctnrs {
		out = append(out, ContainerSummary{
			Name:        c.Names[0],
			ImageName:   c.Image,
			ImageDigest: imgDigestByID[c.ImageID],
		})
	}
	return out, nil
}
