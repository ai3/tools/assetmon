package assetmon

import (
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func debugQueryTest(t *testing.T, uri string) string {
	a, cleanup := createTestAssetMon(t, "testdata/artifacts.sql")
	defer cleanup()

	srv := httptest.NewServer(HTTPAPI(a))
	defer srv.Close()

	resp, err := http.Get(srv.URL + uri)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Fatalf("status: %s", resp.Status)
	}
	dataB, _ := io.ReadAll(resp.Body)
	return string(dataB)
}

func TestService_Debug_Index(t *testing.T) {
	data := debugQueryTest(t, "")
	numRows := strings.Count(data, "<tr")
	if numRows != 3 {
		t.Errorf("index page shows %d rows, expecting 3", numRows)
	}
}

func TestService_Debug_Search(t *testing.T) {
	data := debugQueryTest(t, "?q=pkg:name%3Dbase-files")
	numArtifacts := strings.Count(data, "<tr")
	if numArtifacts != 3 {
		t.Errorf("result shows %d rows, expecting 3", numArtifacts)
	}
}

func TestService_Debug_Image(t *testing.T) {
	data := debugQueryTest(t, "/image?digest=abcdef")
	numRows := strings.Count(data, "<tr")
	// 4 artifact rows, 2 container rows.
	if numRows != 6 {
		t.Errorf("result shows %d rows, expecting 6", numRows)
	}
}
