package assetmon

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
)

const (
	defaultRPCTimeout = 5 * time.Minute
	maxErrors         = 3
	errorRetryDays    = 7
)

// Client for AssetMon, which implements the client side of the image
// scanning workflow. Note that the Context passed to client methods
// should allow for local execution of the scan of potentially
// multiple images, not just the network RPC, so we use a shorter
// timeout for the RPC part.
type Client struct {
	be clientutil.Backend

	rpcTimeout time.Duration
}

// ScanClient needs more information related to scanning. It also
// keeps local state to avoid repeatedly attempting to scan an image
// and fail (for whatever reason).
type ScanClient struct {
	*Client

	hostname string
	stateDB  *sql.DB

	// So we can override it in testing.
	scanFn func(context.Context, string) (*Image, error)
}

func NewAPIClient(be clientutil.Backend) *Client {
	return &Client{
		be:         be,
		rpcTimeout: defaultRPCTimeout,
	}
}

func NewScanClient(be clientutil.Backend, hostname, stateDBPath string) (*ScanClient, error) {
	db, err := OpenStateDB(stateDBPath)
	if err != nil {
		return nil, err
	}
	return &ScanClient{
		Client:   NewAPIClient(be),
		hostname: hostname,
		scanFn:   scanImage,
		stateDB:  db,
	}, nil
}

func (c *ScanClient) Close() {
	c.stateDB.Close()
}

func (c *ScanClient) UpdateContainer(ctx context.Context, cont *ContainerSummary) (bool, error) {
	var resp UpdateResponse
	rpcCtx, cancel := context.WithTimeout(ctx, c.rpcTimeout)
	err := c.be.Call(rpcCtx, "", "/api/v1/update", &UpdateRequest{
		Host:      c.hostname,
		Container: *cont,
	}, &resp)
	cancel()
	if err != nil {
		return false, err
	}

	if len(resp.ScanImages) == 0 {
		return false, nil
	}

	return true, c.processUpdateResponse(ctx, &resp)
}

func (c *ScanClient) FullUpdate(ctx context.Context, ctnrs []ContainerSummary) error {
	log.Printf("updating %d local running containers", len(ctnrs))
	var resp UpdateResponse
	rpcCtx, cancel := context.WithTimeout(ctx, c.rpcTimeout)
	err := c.be.Call(rpcCtx, "", "/api/v1/full_update", &FullUpdateRequest{
		Host:       c.hostname,
		Containers: ctnrs,
	}, &resp)
	cancel()
	if err != nil {
		return err
	}

	return c.processUpdateResponse(ctx, &resp)
}

func (c *ScanClient) processUpdateResponse(ctx context.Context, resp *UpdateResponse) (outErr error) {
	for _, digest := range resp.ScanImages {
		if err := c.conditionallyProcessSingleImage(ctx, digest); err != nil {
			outErr = errors.Join(outErr, fmt.Errorf("%s: %w", digest, err))
		}
	}
	return
}

func (c *ScanClient) processSingleImage(ctx context.Context, digest string) error {
	// Scan the image locally.
	log.Printf("scanning image %s", digest)
	image, err := c.scanFn(ctx, digest)
	if err != nil {
		return fmt.Errorf("failed to scan container image: %w", err)
	}

	// Send report.
	log.Printf("uploading scan report for %s (%d artifacts)", digest, len(image.Artifacts))
	err = c.be.Call(ctx, "", "/api/v1/scan_report", &ScanReportRequest{
		Image: image,
	}, nil)
	if err != nil {
		return fmt.Errorf("failed to upload scan report: %w", err)
	}

	return nil
}

func (c *ScanClient) conditionallyProcessSingleImage(ctx context.Context, digest string) error {
	tx, err := c.stateDB.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}
	defer tx.Rollback()

	retryDeadline := time.Now().AddDate(0, 0, -errorRetryDays)
	if n, lastErr, lastErrTime := c.getErrorCount(tx, digest); n >= maxErrors && lastErrTime.After(retryDeadline) {
		return fmt.Errorf("too many errors (%s)", lastErr)
	}

	err = c.processSingleImage(ctx, digest)

	if sqlErr := c.setError(tx, digest, err); sqlErr != nil {
		log.Printf("warning: failed to save state: %v", sqlErr)
	}
	if sqlErr := tx.Commit(); sqlErr != nil {
		log.Printf("warning: failed to save state: %v", sqlErr)
	}

	return err
}

func (c *ScanClient) getErrorCount(tx *sql.Tx, digest string) (n int, lastErr string, lastErrTime time.Time) {
	_ = tx.QueryRow("SELECT error_count, last_error, last_error_at FROM state WHERE digest = ?", digest).
		Scan(&n, &lastErr, &lastErrTime)
	return
}

func (c *ScanClient) setError(tx *sql.Tx, digest string, opErr error) (err error) {
	if opErr == nil {
		_, err = tx.Exec("INSERT INTO state(digest) VALUES (?) ON CONFLICT(digest) DO UPDATE SET error_count=0, last_error=''", digest)
	} else {
		_, err = tx.Exec("INSERT INTO state(digest, last_error, last_error_at, error_count) VALUES (?, ?, ?, 1) ON CONFLICT(digest) DO UPDATE SET error_count=error_count+1, last_error=?, last_error_at=?", digest, opErr.Error(), time.Now(), opErr.Error(), time.Now())
	}
	return
}

func (c *Client) GetImageSBOM(ctx context.Context, digest string) (*Image, error) {
	var resp GetImageSBOMResponse
	err := c.be.Call(ctx, "", "/api/v1/get_image_sbom", &GetImageSBOMRequest{Digest: digest}, &resp)
	return &resp.Image, err
}
