module git.autistici.org/ai3/tools/assetmon

go 1.15

require (
	git.autistici.org/ai3/go-common v0.0.0-20230816213645-b3aa3fb514d6
	github.com/google/subcommands v1.2.0
	github.com/matttproud/golang_protobuf_extensions v1.0.2-0.20181231171920-c182affec369 // indirect
	gopkg.in/yaml.v3 v3.0.1
)
